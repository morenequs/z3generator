import pika
import json
from solver.Problem import Problem
from solver.ProblemSolver import ProblemSolver
import os
import signal
from contextlib import contextmanager
import time

class TimeoutException(Exception): pass

@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)

def main():
    while True:
        try:
            credentials = pika.PlainCredentials(os.getenv('AMQP_USER'), os.getenv('AMQP_PASS'))
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=os.getenv('AMQP_HOST'), credentials=credentials))
            channel = connection.channel()

            def callback(ch, method, properties, body):
                task = json.loads(body)
                problem = Problem(task['mealPlanTask'], 3)

                type = 'no_plan'
                solution = list()
                try:
                    with time_limit(5):
                        solution = ProblemSolver().solve_problem(problem)
                        if len(solution) > 0:
                            type = 'success'
                except TimeoutException as e:
                    type = 'too_long'

                ch.basic_publish(
                    exchange=os.getenv('AMQP_EXCHANGE_NAME'),
                    routing_key=os.getenv('AMQP_SOLUTION_ROUTING_KEY_NAME'),
                    properties=pika.BasicProperties(
                        headers={'type': 'mealPlanSolution'}
                    ),
                    body=json.dumps({'uuid': task['uuid'], 'mealPlanIds': list(map(int, solution)), 'type': type})
                )

            channel.basic_consume(queue=os.getenv('AMQP_TASK_QUEUE_NAME'), on_message_callback=callback, auto_ack=True)
            channel.start_consuming()
        except:
            print("Unsuccessful connection")
            time.sleep(1)


if __name__ == '__main__':
    main()
