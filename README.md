# Z3 Generator (Meal plan generator)

Current project supports docker only for development environment.

## Requirements:

- docker
- docker-compose
- linux distribution

## Instalation

### Build docker images

Run command in project directory:
```
docker-compose build
```

### First run
Run commands in project directory:
```
docker-compose up -d
```

### Warning

In first order you must have installed backend application (https://gitlab.com/morenequs/easy-diet)