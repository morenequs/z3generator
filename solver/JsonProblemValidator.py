from decimal import *

class JsonProblemValidator:
    def __init__(self, problem, precision):
        self.problem = problem
        self.precision = precision
        getcontext().prec = precision

    def is_valid(self):
        # try:
        # self.__check_number_of_sets()
        self.__check_requirements_definition()
        self.__check_sets()
        # except KeyError as e:
        #     return False, f'Invalid structure. Key {str(e)} does not exist.'
        # except Exception as e:
        #     return False, str(e)
        # except:
        #     return False, None
        #
        # return True

    def __check_requirements_definition(self):
        requirements_definition = self.problem['requirements']
        for key, item in requirements_definition.items():
            if not isinstance(item['min'], str) or not isinstance(item['max'], str):
                raise Exception(f'min and max fields in requirement {key} must be an str')

            print(Decimal(12) > Decimal(11))

            if Decimal(item['min']) * Decimal(10**self.precision) > Decimal(item['max']) * Decimal(10**self.precision):
                raise Exception(f'min can not be greater than max in ${key} requirement')

            if Decimal(item['min']) < Decimal(0):
                raise Exception(f'min can not be less than 0 in ${key} requirement')

    def __check_sets(self):
        requirements_keys_set = set(self.problem['requirements'].keys())
        requirements_keys_set.add('id')
        sets = self.problem['sets']

        if len(sets) < 2 or len(sets) > 6:
            raise Exception(f'Count sets should be between 2 and 6.')

        for set_of_items in sets:
            self.__check_set(set_of_items, requirements_keys_set)

    def __check_set(self, set_of_items, requirements_keys_set):
        for item in set_of_items:
            if not requirements_keys_set.issubset(item.keys()):
                raise Exception(f'Item {str(item)} not contain all requirement keys ({str(requirements_keys_set)})')

            for key in item:
                if key != "id" and not isinstance(item[key], str):
                    raise Exception(f"Key {key} in (item {item}) must be an str")