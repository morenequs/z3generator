from solver.JsonProblemValidator import JsonProblemValidator
from decimal import *

class Problem:
    def __init__(self, problem_json, precision):
        self.precision = precision
        getcontext().prec = precision
        JsonProblemValidator(problem_json, precision).is_valid()

        self.__requirements = self.parseRequirements(problem_json['requirements'].items())
        self.__sets = self.parseSets(problem_json['sets'])
        self.__requirements_keys = problem_json['requirements'].keys

    def parseRequirements(self, requirements):
        def parseRequirement(req):
            return {
                "min": self.parseStringToInt(req['min']),
                "max": self.parseStringToInt(req['max']),
            }

        return dict(map(lambda item: (item[0], parseRequirement(item[1])), requirements))

    def parseSets(self, sets):
        def parseValueExceptId(k, v):
            if k == 'id':
                return v
            return self.parseStringToInt(v)

        def parseItem(item):
            return dict(map(lambda row: (row[0], parseValueExceptId(row[0], row[1])), item.items()))

        def parseSet(set):
            return list(map(lambda set: parseItem(set), set))

        return  list(map(lambda set: parseSet(set), sets))

    def parseStringToInt(self, value):
        return int(Decimal(value) * Decimal(10**self.precision))

    @property
    def requirements(self):
        return self.__requirements

    @property
    def requirements_keys(self):
        return self.__requirements_keys

    @property
    def sets(self):
        return self.__sets