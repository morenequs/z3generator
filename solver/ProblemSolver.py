from z3 import *
import time
import sys

set_option(timeout=6000)

class ProblemSolver:
    def __init__(self):
        self.__solver = Solver()
        self.__problem = None
        self.__item_id_to_item_map = {}
        self.__item_id_to_var_map = {}
        self.__list_of_z3var_item_tuple = []
        self.__all = 0

    def solve_problem(self, problem):
        self.__problem = problem
        self.__produce_item_list()
        self.__apply_requirements()
        return self.__solve()

    def __produce_item_list(self):
        for set_of_items in self.__problem.sets:
            vars = []
            self.__all += len(set_of_items)
            for item in set_of_items:
                var_name = item['id']
                if var_name in self.__item_id_to_var_map:
                    var = self.__item_id_to_var_map[var_name]
                else:
                    var = Bool(var_name)
                    self.__item_id_to_item_map[var_name] = item
                    self.__item_id_to_var_map[var_name] = var
                    self.__list_of_z3var_item_tuple.append((var, item))
                vars.append(var)

            self.__solver.add(PbGe([(x, 1) for x in vars], 0))

        self.__solver.add(PbEq([(x, 1) for x in list(self.__item_id_to_var_map.values())], len(self.__problem.sets)))

    def __apply_requirements(self):
        for requirement_name, requirement_range in self.__problem.requirements.items():
            print(requirement_name)
            print(requirement_range)

            expression = []
            for item in self.__list_of_z3var_item_tuple:
                expression.append((item[0], item[1][requirement_name]))

            self.__solver.add(PbGe(expression, requirement_range['min']), PbLe(expression, requirement_range['max']))

    def __solve(self):
        while self.__solver.check() != CheckSatResult(Z3_L_FALSE):
            model = self.__solver.model()
            requirements_sum = {}
            for requirement_name in self.__problem.requirements:
                requirements_sum[requirement_name] = 0

            result = []

            for x in model:
                if bool(model[x]):
                    result.append(str(self.__item_id_to_item_map[x.name()]['id']))
                    for requirement_name in self.__problem.requirements:
                        requirements_sum[requirement_name] += self.__item_id_to_item_map[x.name()][requirement_name]

                    self.__solver.add(self.__item_id_to_var_map[x.name()] == False)

            return result
        return []
